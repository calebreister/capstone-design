Version 4
SymbolType BLOCK
RECTANGLE Normal 64 48 -64 -32
WINDOW 0 -64 -48 Left 2
WINDOW 3 64 -48 Right 2
SYMATTR Value LM317L
SYMATTR Prefix X
SYMATTR ModelFile Z:\home\caleb\Sync\School\Capstone\design\spice\LM317L.lib
PIN -64 0 LEFT 8
PINATTR PinName IN
PINATTR SpiceOrder 1
PIN 0 48 BOTTOM 8
PINATTR PinName ADJ
PINATTR SpiceOrder 2
PIN 64 0 RIGHT 8
PINATTR PinName OUT
PINATTR SpiceOrder 3
