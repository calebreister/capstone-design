Version 4
SHEET 1 1008 680
WIRE 448 -144 448 -160
WIRE 416 -128 384 -128
WIRE 608 -112 480 -112
WIRE 416 -96 384 -96
WIRE 384 -64 384 -96
WIRE 448 -64 448 -80
WIRE -16 32 -16 16
WIRE -16 32 -80 32
WIRE -80 48 -80 32
WIRE -16 48 -16 32
WIRE 368 128 368 96
WIRE 160 240 128 240
WIRE 224 240 160 240
WIRE 368 240 368 208
WIRE 368 240 304 240
WIRE 432 240 368 240
WIRE 576 240 512 240
WIRE 608 240 576 240
WIRE 160 272 160 240
WIRE 368 272 368 240
WIRE 576 272 576 240
WIRE 160 352 160 336
WIRE 368 352 368 336
WIRE 576 352 576 336
FLAG 128 240 A
IOPIN 128 240 In
FLAG 608 240 B
IOPIN 608 240 Out
FLAG 368 96 W
FLAG 384 -128 W
FLAG 384 -64 0
FLAG 448 -64 -Vs
FLAG 448 -160 +Vs
FLAG 160 352 -Vs
FLAG 368 352 -Vs
FLAG 576 352 -Vs
FLAG 608 -112 B
FLAG -16 128 -Vs
FLAG -16 -64 +Vs
FLAG -80 48 0
FLAG -16 288 0
FLAG -16 208 A
SYMBOL res 320 224 R90
WINDOW 0 0 56 VBottom 2
WINDOW 3 32 56 VTop 2
SYMATTR InstName RWA
SYMATTR Value {20k-Rf}
SYMBOL res 528 224 R90
WINDOW 0 0 56 VBottom 2
WINDOW 3 32 56 VTop 2
SYMATTR InstName RWB
SYMATTR Value {Rf}
SYMBOL cap 144 272 R0
SYMATTR InstName CA
SYMATTR Value 45p
SYMBOL cap 352 272 R0
SYMATTR InstName CW
SYMATTR Value 45p
SYMBOL cap 560 272 R0
SYMATTR InstName CB
SYMATTR Value 45p
SYMBOL voltage -16 -80 R0
SYMATTR InstName V1
SYMATTR Value 5
SYMBOL voltage -16 32 R0
SYMATTR InstName V2
SYMATTR Value 5
SYMBOL voltage -16 192 R0
WINDOW 123 24 120 Left 2
WINDOW 39 0 0 Left 0
SYMATTR Value2 AC 1
SYMATTR InstName V3
SYMATTR Value ""
SYMBOL res 352 112 R0
SYMATTR InstName RW
SYMATTR Value 60
SYMBOL Opamps\\LTC6241 448 -112 R0
SYMATTR InstName U1
TEXT -152 -176 Left 2 ;Capacitor values are for the AD528x RDAC
TEXT -104 448 Left 2 !.ac dec 100 1 100Meg
TEXT -104 472 Left 2 !.step param Rf list 10k 13437.50 18203.13 19062.50 19609.38 19921.88
