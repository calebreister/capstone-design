Version 4
SymbolType BLOCK
LINE Normal 16 -16 -16 -16
LINE Normal 27 -26 16 -16
LINE Normal -16 -16 -27 -6
LINE Normal -21 16 0 -16
LINE Normal 21 16 -21 16
LINE Normal 0 -16 21 16
LINE Normal 0 -32 0 -16
LINE Normal 0 16 0 32
LINE Normal 32 0 11 0
RECTANGLE Normal 32 32 -32 -32
WINDOW 3 -40 16 Right 2
WINDOW 0 -40 -16 Right 2
SYMATTR Value ShuntRef
SYMATTR Prefix X
SYMATTR Description 2.5V Adjustable Reference
PIN 0 -32 NONE 8
PINATTR PinName Cathode
PINATTR SpiceOrder 1
PIN 0 32 NONE 8
PINATTR PinName Anode
PINATTR SpiceOrder 2
PIN 32 0 NONE 8
PINATTR PinName Ref
PINATTR SpiceOrder 3
